# HTML_TEMPLATE
`İşlənəçək əsas qovluq src qovluqudur. JavaScript və SASS fayları src qovluqunun içərsindədir`

## NPM modullarının qurulması və işə salınması

#### Module yükləmə
`npm run install`

#### Development prosesində build etmə
`npm run watch`

#### Production prosesində build etmə
`npm run dev`

### Build olan fayllar assets qovluquna köcürülür

#### JavaScript 
`assets/js/script.js`
`assets/js/app.js`

#### Css 
`assets/css/app.css`
`assets/css/desktop.css`
`assets/css/mobile.css`
